//
//  Initializer.cpp
//  rattle
//
//  Created by George Walker on 10/21/14.
//  Copyright (c) 2014 Finster. All rights reserved.
//

#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <tuple>
#include <exception>
#include <math.h>

#include <boost/algorithm/string.hpp>

// for std::copy
//#include <algorithm>
//#include <iterator>

#include "Environment.h"
#include "Constants.h"

using namespace std;

//string Environment::eqtype, Environment::eqsize;
//int Environment::Nx, Environment::Nz, Environment::iring, Environment::icond;
//float Environment::tempev, Environment::q, Environment::xmin, Environment::xmax;
//float Environment::zmin, Environment::zmax, Environment::vleft, Environment::vright, Environment::vwall;
//float Environment::zcutlef, Environment::zcutrgt, Environment::rcut;
//vector<float> Environment::zleft, Environment::zright, Environment::vring;

Environment::Environment()
{
	if (this->skipsteps == 1)
	{
		this->Nsteps2 = this->Trun / this->skipsteps;
	}
	else
	{
		this->Nsteps2 = 1 + (this->Trun / this->skipsteps);
	}
	
	// update the mass and charge
	this->m = this->m * this->scalepart;
	this->q = this->q * this->scalepart;

}

Environment::~Environment()
{
	// delete phi and dens?
}

tuple<vector<vector<double>>, vector<vector<double>>> Environment::ReadEq()
{
	// read the newquilibrium.fil file
	ifstream newequil ("newequilibrium.fil");
	string line;
	
	getline(newequil, line); // eqtype,eqsize
	vector<string> tokens = TokensFromString(line);
	eqtype = tokens[1];
	eqsize = tokens[2];
	
	getline(newequil, line); // Nx, Nz, tempev, q
	tokens = TokensFromString(line);
	Nx = stoi(tokens[1]);
	Nz = stoi(tokens[2]);
	tempev = stof(tokens[3]);
	q = stof(tokens[4]);
	
	getline(newequil, line); // xmin, xmax
	tokens = TokensFromString(line);
	this->xmin = stof(tokens[1]);
	this->xmax = stof(tokens[2]);
	
	getline(newequil, line); // zmin, zmax
	tokens = TokensFromString(line);
	this->zmin = stof(tokens[1]);
	this->zmax = stof(tokens[2]);
	
	getline(newequil, line); // Vleft, Vright, Vwall
	tokens = TokensFromString(line);
	this->vleft = stof(tokens[1]);
	this->vright = stof(tokens[2]);
	this->vwall = stof(tokens[3]);
	
	getline(newequil, line); // Zcut(left), Zcut(right), and Rcut
	tokens = TokensFromString(line);
	this->zcutlef = stof(tokens[1]);
	this->zcutrgt = stof(tokens[2]);
	this->rcut = stof(tokens[3]);
	
	getline(newequil, line); // # of rings
	tokens = TokensFromString(line);
	this->iring = stoi(tokens[1]);
	if (this->iring > ::ndimcon) {
		throw exception (); // TODO: specify exception: iring value is too big for ndimcon; recompile
	}
	if (this->iring != 0) {
		this->zleft = vector<double>(iring);
		this->zright = vector<double>(iring);
		this->vring = vector<double>(iring);
		for (int i = 0; i < iring; ++i) {
			getline(newequil, line);
			tokens = TokensFromString(line);
			this->zleft[i] = stof(tokens[i+1]);
			this->zright[i] = stof(tokens[i+1]);
			this->vring[i] = stof(tokens[i+1]);
		}
	}
	
	getline(newequil, line);
	tokens = TokensFromString(line);
	this->icond = stoi(tokens[1]);
	
	// now we read Nz sets of Nx elements
	// TODO: Put these on the heap?
	vector<vector<double>> phi;
	vector<vector<double>> dens;
	for (int z = 1; z <= Nz; ++z) {
		cout << "z:" << z << "\n";
		getline(newequil, line); // the "jz=   z: Phi, then Density" line
		// phi
		phi.push_back(vector<double>());
		for (int x = 0; x < Nx; x+=5) {
			//cout << "phi x:" << x+1 << "-" << x+5 << "\n";
			getline(newequil, line);
			tokens = TokensFromString(line);
			for (int i = 1; i <= 5; ++i) {
				phi[z-1].push_back(stod(tokens[i]));
			}
		}
		// dens
		dens.push_back(vector<double>());
		for (int x = 0; x < Nx; x+=5) {
			//cout << "dens x:" << x+1 << "-" << x+5 << "\n";
			getline(newequil, line);
			tokens = TokensFromString(line);
			for (int i = 1; i <= 5; ++i) {
				dens[z-1].push_back(stod(tokens[i]));
			}
		}
	}
	
	// lastly, need to get the penning freq
	getline(newequil, line); // omgpen
	tokens = TokensFromString(line);
	this->omgpen = stof(tokens[1]);
	
	//std::copy(tokens.begin(), tokens.end(), std::ostream_iterator<string>(std::cout, "\n"));
	
	return make_tuple (phi, dens);
}

vector<string> Environment::TokensFromString(string toTokenize)
{
	vector<string> tokenList;
	split(tokenList, toTokenize, boost::is_any_of(" ,'"), boost::token_compress_on);
	return tokenList;
}

void Environment::SetupCRing(float cringLength)
{
	this->Cring = cringLength;
	this->NC = std::floor(Cring / this->dz);
	this->Cright = this->zleft[this->zleft.size() - 1];
	this->Cleft = this->Cright - this->Cring;
	this->NCleft = std::ceil((this->Cleft - this->zmin) / this->dz);
	this->NCright = NCleft + NC;
}