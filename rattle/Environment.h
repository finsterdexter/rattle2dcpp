//
//  Initializer.h
//  rattle
//
//  Created by George Walker on 10/21/14.
//  Copyright (c) 2014 Finster. All rights reserved.
//

#ifndef __rattle__Initializer__
#define __rattle__Initializer__

#include <stdio.h>
#include <math.h>
#include <tuple>
#include <string>
#include <vector>

class Environment
{
private:
	std::vector<std::string> TokensFromString(std::string toTokenize);
	
public:
	Environment();
	~Environment();
	
	// fields
	// these fields all get set during ReadEq()
	std::string eqtype, eqsize;
	int Nx, Nz, iring, icond;
	double tempev, xmin, xmax, zmin, zmax, vleft, vright, vwall, zcutlef, zcutrgt, rcut, omgpen;
	std::vector<double> zleft,zright,vring;
	
	// Cring stuff
	double Cring, NC, Cright, Cleft, NCleft, NCright;
	
	// this is returning two arrays, phi (potential) and dens (density)
	std::tuple<std::vector<std::vector<double>>, std::vector<std::vector<double>>> ReadEq();
	
	// Set up C ring data
	void SetupCRing(float cringLength);
	
	// All of the Constants we will need
	double m = 1.165189e-26;
	double q = 1.60217646e-19;
	
	//const int Nx = 100; // dimensions of grid
	//const int Nz = 400; // dimensions of grid
	const int ndimcon = 2; // number of rings/conductors (?)
	
	const int iter = 1; // number of times restart loop has run
	
	// Elementary constants
	//extern double m; // Beryllium Mass || 1.6726*10^(-27), ! Mass of Proton
	//extern double q; // charge of the particles
	const double e0 = 8.85418781762e-12; // permetivity of free space
	
	// Magnetic field
	const double Bfield = 0.2144; // Magnetic field
	const double wc = q * Bfield / m; // cyclotron frequency
	const double Tc = 2 * M_PI / wc; // Cyclotron period
	const double omgfrac = (.754 + .1 * iter) * wc; // driving freq
	// double omgfrac = .01 * iter * wc; // fraction of wc used in driving potentials
	
	// Drive voltage & time
	const int Ncycle = 20; // # of time steps per cyclotron orbit
	const double Vdrive = 5.0; // driving potential on endcaps
	const int Tdrive = 50 * Ncycle; // # of time steps spent driving plasma
	
	// Here is a list of 1D modes for Temp=.2 eV for reference
	// Mode  omega/omega_c   k_r*a (where a=.02)
	// 3H       .8583         12.1663
	// 2H       .8368          8.6240
	// 1H       .8082          5.2266
	// 0        .7570           .0794
	// 1L       .7031          4.9331
	// 2L       .6718          7.8183
	// 3L       .6422         10.6618
	
	// Radial Drive
	const double Eamp = 10.0; // amplitude of the radial drive
	const double kr = .0794 / .02; // radial wave number for seeding higher modes
	
	// if Rseed = 0 then no radial drive. If Rseed=1 then yes there is a radial drive
	const bool Rseed = false;
	
	// Total run time
	const int Trun = 1000 * Ncycle; // # of time steps to run after the drive
	const double dt = Tc / Ncycle; // time step length
	const int Ttotal = Trun + Tdrive; // total # of time steps for each run
	
	// Spacial constants
	//const double xmin = 0; // inner width of plasma
	//const double xmax = pow(0.04, 2.0); // outer width of plasma
	const double Width = xmax - xmin; // width of total region
	//const double zmin = -.22; // left edge
	//const double zmax =  .22; // right edge
	const double Length = zmax - zmin; // length of total region
	const double dx = Width / (Nx - 1); // Distance between nodes in the x direction
	const double dz = Length / (Nz - 1); // Distance between nodes in the z direction
	const double Vol = M_PI * dx * dz; // This is the volume for each cell
	
	const int scalepart = 1e3; // # of physical particles per simulated ones
	const int skipsteps = 1; // # of times to skip between saving certain data
	
	const int Nsteps1 = Trun - Tdrive;
	int Nsteps2;
	
	
	
	// Brillouin limit related info
	const double beta = 0.85; // fraction of Brillouin limit
	const double w0 = wc / 2 * (1 - sqrt(1 - beta)); // equilibrium rotation frequency when close to Brillouin limit
	
	// Frequently used constants
	const double const1 = .25 / e0;
	const double const2 = .25 / pow(dz, 2);
	const double const3 = 1/dx;
	const double const4 = pow(const3, 2);
	
	// Cutoff limits for the data output files (i.e. cut out parts with no particles to make the data files smaller)
	const int xouterlim = 2 * Nx / 5;
	const int zleftlim = 3 * Nz / 10;
	const int zrightlim = 7 * Nz / 10;

};

#endif /* defined(__rattle__Initializer__) */
