//
//  FileWriter.cpp
//  rattle
//
//  Created by George Walker on 11/10/14.
//  Copyright (c) 2014 Finster. All rights reserved.
//

#include <math.h>
#include <stdio.h>
#include <string>
#include <boost/format.hpp>
#include "FileWriter.h"

FileWriter::FileWriter(Environment env)
{
	// setup to write individual filenames; line 226
	this->ifilefreq = round(env.omgfrac / env.wc * 10000);
	this->ifiledrive = env.Tdrive / env.Ncycle;
	this->ifilevolt = round(env.Vdrive);
	this->ifileEamp = round(env.Eamp);
	this->ifileka = round(env.kr * .02 * 1000);
	

	// TODO: change these variable names to be more semantic
	std::string filename1, filename2, filename3, filename4;
	if (env.Rseed == true)
	{
		std::string filename = str(boost::format("%dx%dgrid%02dvolts%05dwc%02ddrive%02dncycle.dat") % env.Nx % env.Nz % ifilevolt % ifilefreq % ifiledrive % env.Ncycle);
		filename1 = "vr" + filename;
		filename2 = "vz" + filename;
		filename3 = "fft" + filename;
		filename4 = "rho" + filename;
	}
	else
	{
		std::string filename = str(boost::format("%dx%dgrid%02damp%05dka%02dwc%02ddrive%02dncycle.dat") % env.Nx % env.Nz % ifileEamp % ifileka % ifilefreq % ifiledrive % env.Ncycle);
		filename1 = "VR" + filename;
		filename2 = "VZ" + filename;
		filename3 = "FFT" + filename;
		filename4 = "RHO" + filename;
	}

	this->vrfilename = filename1;
	this->vzfilename = filename2;
	this->fftfilename = filename3;
	this->rhofilename = filename4;
	
	// clean up any old files with same names
	remove(this->vrfilename.c_str());
	remove(this->vzfilename.c_str());
	remove(this->fftfilename.c_str());
	remove(this->rhofilename.c_str());
}

FileWriter::~FileWriter()
{
	
}