//
//  FileWriter.h
//  rattle
//
//  Created by George Walker on 11/10/14.
//  Copyright (c) 2014 Finster. All rights reserved.
//

#ifndef __rattle__FileWriter__
#define __rattle__FileWriter__

#include <stdio.h>
#include <string>
#include "Environment.h"

// this class manages any file output
class FileWriter
{
private:
	int ifilefreq, ifiledrive, ifilevolt, ifileEamp, ifileka;
	std::string vrfilename, vzfilename, fftfilename, rhofilename; // filename1, 2, 3, 4
	
public:
	FileWriter(Environment env);
	~FileWriter();
};


#endif /* defined(__rattle__FileWriter__) */
