//
//  main.cpp
//  rattle
//
//  Created by George Walker on 10/20/14.
//  Copyright (c) 2014 Finster. All rights reserved.
//

#include <iostream>
#include <tuple>
//#include "Constants.h"
#include "Environment.h"
#include "FileWriter.h"

int main(int argc, const char * argv[]) {
	Environment env;
	float cringLength = 0.4;
	
	// read the equilibrium file
	std::vector<std::vector<double>> phi;
	std::vector<std::vector<double>> dens;

	std::tie (phi,dens) = env.ReadEq(); // line 211
	
	// C ring data (measures the surface charge density for FFT analysis; line 218
	env.SetupCRing(cringLength);
	
	// setup to write individual filenames; line 226
	FileWriter filewriter = FileWriter(env);
	
	std::cout << "Hello, World!\n";
	//std::cout << Environment::eqtype << "\n";
	//std::cout << Environment::eqsize << "\n";
    return 0;
}
