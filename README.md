# README #

The purpose of this project is to port REALLY old Fortran code over to C++, and once that is done, start looking at ways to better parallelize the code.

The original Fortran code is written and maintained by the Plasma Physics research group at BYU.